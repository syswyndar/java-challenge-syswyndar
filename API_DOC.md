## Java Challenge Documentation API


#### Base URL : 

<details>
 <summary><code>POST</code> <code><b>/</b></code> <code>user</code> <code><b>/</b></code> <code>register</code></summary>

##### Body

> | name      |  type     | data type               | description                                                           |
> |-----------|-----------|-------------------------|-----------------------------------------------------------------------|
> | username  |  required | object (JSON or YAML)   | N/A  |
> | password  |  required | object (JSON or YAML)   | N/A  |


##### Responses

> | http code     | content-type                      | response                                                            |
> |---------------|-----------------------------------|---------------------------------------------------------------------|
> | `201`         | `text/plain;charset=UTF-8`        | `Sukses Register`                                |
> | `409`         | `text/plain;charset=UTF-8`        | `Username Sudah Terpakai`                        |
> | `500`         | `text/plain;charset=UTF-8`        | `Internal Server Error`                          |

</details>

<details>
<summary><code>POST</code> <code><b>/</b></code> <code>user</code> <code><b>/</b></code> <code>login</code></summary>

##### Body

> | name      |  type     | data type               | description                                                           |
> |-----------|-----------|-------------------------|-----------------------------------------------------------------------|
> | username  |  required | object (JSON or YAML)   | N/A  |
> | password  |  required | object (JSON or YAML)   | N/A  |


##### Responses

> | http code     | content-type                      | response                                                            |
> |---------------|-----------------------------------|---------------------------------------------------------------------|
> | `200`         | `text/plain;charset=UTF-8`        | `Sukses Login`                                |
> | `401`         | `text/plain;charset=UTF-8`        | `Username / Password Tidak Cocok.`            |
> | `403`         | `text/plain;charset=UTF-8`        | `User Tidak Ditemukan`                        |
> | `500`         | `text/plain;charset=UTF-8`        | `Internal Server Error`                       |

</details>

<details>
<summary><code>PUT</code> <code><b>/</b></code> <code>user</code> <code><b>/</b></code> <code>{:id}</code></summary>

##### Body

> | name      |  type     | data type               | description                                                           |
> |-----------|-----------|-------------------------|-----------------------------------------------------------------------|
> | username  |  required | object (JSON or YAML)   | N/A  |
> | password  |  required | object (JSON or YAML)   | N/A  |


##### Responses

> | http code     | content-type                      | response                                                            |
> |---------------|-----------------------------------|---------------------------------------------------------------------|
> | `201`         | `text/plain;charset=UTF-8`        | `Sukses Update User`                                                      |
> | `400`         | `text/plain;charset=UTF-8`        | `Password tidak boleh sama dengan password sebelumnya`              |
> | `401`         | `text/plain;charset=UTF-8`        | `Username Sudah Terpakai`                                           |
> | `409`         | `text/plain;charset=UTF-8`        | `Username Sudah Terpakai`                                           |
> | `500`         | `text/plain;charset=UTF-8`        | `Internal Server Error`                                             |

</details>

<details>
<summary><code>GET</code> <code><b>/</b></code> <code>user</code> <code><b>/</b></code> <code>list</code></summary>

##### Body

##### Responses

> | http code     | content-type                      | response                                                            |
> |---------------|-----------------------------------|---------------------------------------------------------------------|
> | `200`         | `application/json`                | `[{username: string, password: string}]`                            |
> | `500`         | `text/plain;charset=UTF-8`        | `Internal Server Error`                                             |

</details>