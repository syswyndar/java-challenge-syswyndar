package ist.challenge.sys_wyndar;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import ist.challenge.sys_wyndar.controllers.UserController;
import ist.challenge.sys_wyndar.model.User;
import ist.challenge.sys_wyndar.service.UserService;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserController.class)
// @AutoConfigureMockMvc
public class UserLoginTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserService userService;

    @Test
    public void testShouldReturn200Success() throws Exception {
        User user = new User();
        user.setId(Long.valueOf(1));
        user.setUsername("sys");
        user.setPassword("password");

        User newUser = new User();
        newUser.setUsername("sys");
        newUser.setPassword("password");

        String reqBody = objectMapper.writeValueAsString(newUser);

        Mockito.when(userService.getUserByBody(newUser)).thenReturn(user);

        mockMvc.perform(MockMvcRequestBuilders.post("/user/login")
                .contentType("application/json")
                .content(reqBody))
                .andExpect(status().isOk());

    }
}
