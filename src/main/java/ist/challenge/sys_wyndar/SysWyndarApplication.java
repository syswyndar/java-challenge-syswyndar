package ist.challenge.sys_wyndar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SysWyndarApplication {

	public static void main(String[] args) {
		SpringApplication.run(SysWyndarApplication.class, args);
	}
}
