package ist.challenge.sys_wyndar.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ist.challenge.sys_wyndar.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
