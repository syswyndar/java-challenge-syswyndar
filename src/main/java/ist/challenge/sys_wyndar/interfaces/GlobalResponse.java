package ist.challenge.sys_wyndar.interfaces;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GlobalResponse<T> {

    private int code;
    private String message;
    private T data;
}
