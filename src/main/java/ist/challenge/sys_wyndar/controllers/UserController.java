package ist.challenge.sys_wyndar.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ist.challenge.sys_wyndar.interfaces.GlobalResponse;
import ist.challenge.sys_wyndar.model.User;
import ist.challenge.sys_wyndar.service.UserService;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/testserver")
    public ResponseEntity<?> testServer() {
        GlobalResponse resp = new GlobalResponse<>();
        resp.setCode(200);
        resp.setMessage("Server Successfully Running");

        return ResponseEntity.ok(resp);
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody User req) {
        try {
            // validate user
            User reqData = new User();
            reqData.setUsername(req.getUsername());

            User user = userService.getUserByBody(reqData);
            if (user != null) {
                return ResponseEntity.status(409).body("Username Sudah Terpakai");
            }

            user = userService.saveUser(req);
            return ResponseEntity.status(HttpStatus.CREATED).body("Sukses Register");

        } catch (Exception e) {
            return ResponseEntity.internalServerError().body("Internal Server Error");
        }
    }

    @PostMapping("/login")
    public ResponseEntity<?> loginUser(@RequestBody User req) {
        try {
            // validate user
            User reqData = new User();
            reqData.setUsername(req.getUsername());

            User user = userService.getUserByBody(reqData);
            if (user == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User Tidak Ditemukan");
            }

            if (!user.getUsername().equals(req.getUsername()) || !user.getPassword().equals(req.getPassword())) {
                return ResponseEntity.status(401).body("Username / Password Tidak Cocok.");
            }

            return ResponseEntity.ok("Sukses Login");

        } catch (Exception e) {
            return ResponseEntity.internalServerError().body("Internal Server Error");
        }
    }

    @GetMapping("/list")
    public ResponseEntity<?> getListUser() {
        try {
            List<User> users = userService.getListUser();

            return ResponseEntity.ok(users);

        } catch (Exception e) {
            return ResponseEntity.internalServerError().body("Internal Server Error");
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateUser(@PathVariable Long id, @RequestBody User req) {
        try {
            User user = userService.getUserById(id);

            if (user == null) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User Tidak Ditemukan");
            }

            // validate username
            User userData = new User();
            userData.setUsername(req.getUsername());
            userData = userService.getUserByBody(userData);

            if (userData != null) {
                return ResponseEntity.status(409).body("Username sudah terpakai");
            }

            // validate password
            if (user.getPassword().equals(req.getPassword())) {
                return ResponseEntity.status(400).body("Password tidak boleh sama dengan password sebelumnya");
            }

            user.setUsername(req.getUsername());
            user.setPassword(req.getPassword());

            user = userService.saveUser(user);

            return ResponseEntity.status(HttpStatus.CREATED).body("Sukses Update User");
        } catch (Exception e) {
            return ResponseEntity.internalServerError().body("Internal Server Error");
        }
    }

}
