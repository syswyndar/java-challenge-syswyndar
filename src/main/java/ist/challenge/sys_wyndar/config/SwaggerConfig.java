package ist.challenge.sys_wyndar.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
// @EnableWebMvc
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfoMetaData())
                .select()
                .apis(RequestHandlerSelectors.basePackage("ist.challenge"))
                .build();
    }

    private ApiInfo apiInfoMetaData() {
        return new ApiInfoBuilder().title("Java Challenge Documentation")
                .description("Documentation Api For Java Challenge Sys_Wyndar")
                .version("1.0.0")
                .build();
    }
}
