package ist.challenge.sys_wyndar.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import ist.challenge.sys_wyndar.model.User;
import ist.challenge.sys_wyndar.repository.UserRepository;
import jakarta.transaction.Transactional;

@Service
public class UserService {

    @Autowired
    UserRepository repo;

    public User saveUser(User req) {
        return repo.save(req);
    }

    public User getUserById(Long id) {
        return repo.findById(id).orElse(null);
    }

    public User getUserByBody(User req) {
        Example<User> role = Example.of(req);
        Optional<User> user = repo.findOne(role);

        if (user.isPresent()) {
            return user.get();
        }

        return null;
    }

    @Transactional
    public void deleteUser(Long id) {
        repo.deleteById(id);
    }

    public List<User> getListUser() {
        return repo.findAll();
    }
}
